import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UserInterfaceFrameworkPage } from './user-interface-framework.page';

describe('UserInterfaceFrameworkPage', () => {
  let component: UserInterfaceFrameworkPage;
  let fixture: ComponentFixture<UserInterfaceFrameworkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserInterfaceFrameworkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UserInterfaceFrameworkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
