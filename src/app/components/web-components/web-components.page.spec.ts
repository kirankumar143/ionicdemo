import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WebComponentsPage } from './web-components.page';

describe('WebComponentsPage', () => {
  let component: WebComponentsPage;
  let fixture: ComponentFixture<WebComponentsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebComponentsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WebComponentsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
