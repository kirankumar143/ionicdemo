import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GPUPageRoutingModule } from './gpu-routing.module';

import { GPUPage } from './gpu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GPUPageRoutingModule
  ],
  declarations: [GPUPage]
})
export class GPUPageModule {}
