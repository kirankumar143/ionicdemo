import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppStoresPage } from './app-stores.page';

const routes: Routes = [
  {
    path: '',
    component: AppStoresPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppStoresPageRoutingModule {}
