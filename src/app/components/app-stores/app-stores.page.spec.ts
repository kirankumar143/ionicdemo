import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AppStoresPage } from './app-stores.page';

describe('AppStoresPage', () => {
  let component: AppStoresPage;
  let fixture: ComponentFixture<AppStoresPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppStoresPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AppStoresPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
