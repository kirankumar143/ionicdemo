import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  },
 
  {
    path: 'fundamentals',
    loadChildren: () => import('./components/fundamentals/fundamentals.module').then( m => m.FundamentalsPageModule)
  },
  {
    path: 'ionic-framework',
    loadChildren: () => import('./components/ionic-framework/ionic-framework.module').then( m => m.IonicFrameworkPageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./components/menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'apache-cordova',
    loadChildren: () => import('./components/apache-cordova/apache-cordova.module').then( m => m.ApacheCordovaPageModule)
  },
  {
    path: 'web-components',
    loadChildren: () => import('./components/web-components/web-components.module').then( m => m.WebComponentsPageModule)
  },
  {
    path: 'user-interface-framework',
    loadChildren: () => import('./components/user-interface-framework/user-interface-framework.module').then( m => m.UserInterfaceFrameworkPageModule)
  },
  {
    path: 'app-stores',
    loadChildren: () => import('./components/app-stores/app-stores.module').then( m => m.AppStoresPageModule)
  },
  {
    path: 'cordova-vs-capacitor',
    loadChildren: () => import('./components/cordova-vs-capacitor/cordova-vs-capacitor.module').then( m => m.CordovaVsCapacitorPageModule)
  },
  {
    path: 'angular',
    loadChildren: () => import('./components/angular/angular.module').then( m => m.AngularPageModule)
  },
  {
    path: 'command-line-interface',
    loadChildren: () => import('./components/command-line-interface/command-line-interface.module').then( m => m.CommandLineInterfacePageModule)
  },
  {
    path: 'npm-module',
    loadChildren: () => import('./components/npm-module/npm-module.module').then( m => m.NpmModulePageModule)
  },
  {
    path: 'gpu',
    loadChildren: () => import('./components/gpu/gpu.module').then( m => m.GPUPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
