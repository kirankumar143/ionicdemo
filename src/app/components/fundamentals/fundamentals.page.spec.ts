import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FundamentalsPage } from './fundamentals.page';

describe('FundamentalsPage', () => {
  let component: FundamentalsPage;
  let fixture: ComponentFixture<FundamentalsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FundamentalsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FundamentalsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
