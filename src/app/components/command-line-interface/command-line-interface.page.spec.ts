import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CommandLineInterfacePage } from './command-line-interface.page';

describe('CommandLineInterfacePage', () => {
  let component: CommandLineInterfacePage;
  let fixture: ComponentFixture<CommandLineInterfacePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommandLineInterfacePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CommandLineInterfacePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
