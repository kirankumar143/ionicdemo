import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AppStoresPageRoutingModule } from './app-stores-routing.module';

import { AppStoresPage } from './app-stores.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AppStoresPageRoutingModule
  ],
  declarations: [AppStoresPage]
})
export class AppStoresPageModule {}
