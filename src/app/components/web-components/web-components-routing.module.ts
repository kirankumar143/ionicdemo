import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WebComponentsPage } from './web-components.page';

const routes: Routes = [
  {
    path: '',
    component: WebComponentsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WebComponentsPageRoutingModule {}
