import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GPUPage } from './gpu.page';

describe('GPUPage', () => {
  let component: GPUPage;
  let fixture: ComponentFixture<GPUPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GPUPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GPUPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
