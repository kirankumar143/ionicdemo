import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WebComponentsPageRoutingModule } from './web-components-routing.module';

import { WebComponentsPage } from './web-components.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WebComponentsPageRoutingModule
  ],
  declarations: [WebComponentsPage]
})
export class WebComponentsPageModule {}
