import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IonicFrameworkPageRoutingModule } from './ionic-framework-routing.module';

import { IonicFrameworkPage } from './ionic-framework.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicFrameworkPageRoutingModule
  ],
  declarations: [IonicFrameworkPage]
})
export class IonicFrameworkPageModule {}
