import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ApacheCordovaPageRoutingModule } from './apache-cordova-routing.module';

import { ApacheCordovaPage } from './apache-cordova.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ApacheCordovaPageRoutingModule
  ],
  declarations: [ApacheCordovaPage]
})
export class ApacheCordovaPageModule {}
