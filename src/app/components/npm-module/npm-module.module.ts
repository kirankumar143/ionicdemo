import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NpmModulePageRoutingModule } from './npm-module-routing.module';

import { NpmModulePage } from './npm-module.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NpmModulePageRoutingModule
  ],
  declarations: [NpmModulePage]
})
export class NpmModulePageModule {}
