import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NpmModulePage } from './npm-module.page';

describe('NpmModulePage', () => {
  let component: NpmModulePage;
  let fixture: ComponentFixture<NpmModulePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NpmModulePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NpmModulePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
