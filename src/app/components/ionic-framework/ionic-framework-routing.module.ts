import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IonicFrameworkPage } from './ionic-framework.page';

const routes: Routes = [
  {
    path: '',
    component: IonicFrameworkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IonicFrameworkPageRoutingModule {}
