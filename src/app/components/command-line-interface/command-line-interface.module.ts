import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CommandLineInterfacePageRoutingModule } from './command-line-interface-routing.module';

import { CommandLineInterfacePage } from './command-line-interface.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CommandLineInterfacePageRoutingModule
  ],
  declarations: [CommandLineInterfacePage]
})
export class CommandLineInterfacePageModule {}
