import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommandLineInterfacePage } from './command-line-interface.page';

const routes: Routes = [
  {
    path: '',
    component: CommandLineInterfacePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CommandLineInterfacePageRoutingModule {}
