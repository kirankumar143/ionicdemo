import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApacheCordovaPage } from './apache-cordova.page';

const routes: Routes = [
  {
    path: '',
    component: ApacheCordovaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApacheCordovaPageRoutingModule {}
