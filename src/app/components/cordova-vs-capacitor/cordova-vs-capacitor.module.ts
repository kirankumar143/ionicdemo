import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CordovaVsCapacitorPageRoutingModule } from './cordova-vs-capacitor-routing.module';

import { CordovaVsCapacitorPage } from './cordova-vs-capacitor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CordovaVsCapacitorPageRoutingModule
  ],
  declarations: [CordovaVsCapacitorPage]
})
export class CordovaVsCapacitorPageModule {}
