import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CordovaVsCapacitorPage } from './cordova-vs-capacitor.page';

describe('CordovaVsCapacitorPage', () => {
  let component: CordovaVsCapacitorPage;
  let fixture: ComponentFixture<CordovaVsCapacitorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CordovaVsCapacitorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CordovaVsCapacitorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
