import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GPUPage } from './gpu.page';

const routes: Routes = [
  {
    path: '',
    component: GPUPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GPUPageRoutingModule {}
