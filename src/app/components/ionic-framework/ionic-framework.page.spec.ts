import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IonicFrameworkPage } from './ionic-framework.page';

describe('IonicFrameworkPage', () => {
  let component: IonicFrameworkPage;
  let fixture: ComponentFixture<IonicFrameworkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IonicFrameworkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IonicFrameworkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
