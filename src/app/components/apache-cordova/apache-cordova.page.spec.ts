import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ApacheCordovaPage } from './apache-cordova.page';

describe('ApacheCordovaPage', () => {
  let component: ApacheCordovaPage;
  let fixture: ComponentFixture<ApacheCordovaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApacheCordovaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ApacheCordovaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
