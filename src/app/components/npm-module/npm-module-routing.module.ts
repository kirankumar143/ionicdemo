import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NpmModulePage } from './npm-module.page';

const routes: Routes = [
  {
    path: '',
    component: NpmModulePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NpmModulePageRoutingModule {}
