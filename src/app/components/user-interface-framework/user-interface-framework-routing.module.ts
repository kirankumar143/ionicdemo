import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserInterfaceFrameworkPage } from './user-interface-framework.page';

const routes: Routes = [
  {
    path: '',
    component: UserInterfaceFrameworkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserInterfaceFrameworkPageRoutingModule {}
